# OpenML dataset: Electronic-Music-Features---201802-BeatportTop100

https://www.openml.org/d/43690

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Electronic dance music (EDM) is a genre where thousands of new songs are released every week. The list of EDM subgenres considered is long, but it also evolves according to trends and musical tastes. 
With this in view, we have retrieved two sets of over 2,000 songs separated by more than a year. Songs belong to the top 100 list of an EDM website taxonomy of more than 20 subgenres that changed in the period considered.
Content
Each row is an electronic music song. The dataset contains 100 song for each genre among Beatport electronic music genres, they were the top (100) songs of their genres on November 2018. Columns are audio features extracted of a two random minutes sample of the file audio. These features have been extracted using pyAudioAnalysis (https://github.com/tyiannak/pyAudioAnalysis).
Acknowledgements
Special thanks to the people who made this possible. Javier Arroyo, Laura Prez-Molina y Jaime Snchez-Hernndez.
Inspiration
These datasets are used in this publication "Automatic subgenre classication in an electronic dance music taxonomy
" where we test the effectiveness of automatic classification on these sets and delve into the results.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43690) of an [OpenML dataset](https://www.openml.org/d/43690). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43690/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43690/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43690/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

